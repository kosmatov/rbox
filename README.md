rbox
====

Vagrant box for software development provisioned by Ansible

### Installation

* [Download](https://www.vagrantup.com/downloads.html) and install `vagrant`
* Clone this repo: `git clone git://github.com/kosmatov/rbox.git && cd rbox`
* Create new branch: `git checkout -b my-box`
* Comment lines with tasks that you needn't in `roles/common/main.yml` & `roles/development/main.yml`
* Download JDK from [here](http://www.oracle.com/technetwork/java/javase/downloads/index.html) and put it into `vendor/` directory
* Run `vagrant up`

### What inside

<a href="https://centos.org" title="CentOS"><img src="https://github.com/kosmatov/rbox/blob/master/samples/centos-logo.png" height="100"></a>
<a href="https://zsh.org" title="ZSH"><img src="https://github.com/kosmatov/rbox/blob/master/samples/zsh-logo.png" height="100"></a>
<a href="https://github.com/robbyrussell/oh-my-zsh" title="Oh-my-zsh"><img src="https://github.com/kosmatov/rbox/blob/master/samples/oh-my-zsh.png" height="100"></a>
<a href="https://git-scm.com" title="Git"><img src="https://github.com/kosmatov/rbox/blob/master/samples/git-logo.png" height="100"></a>
<a href="https://github.com/nvie/gitflow" title="Git-Flow"><img src="https://github.com/kosmatov/rbox/blob/master/samples/gitflow-logo.png" height="100"></a>
<a href="https://neovim.io" title="Neovim"><img src="https://github.com/kosmatov/rbox/blob/master/samples/neovim-logo.png" height="100"></a>
<a href="https://github.com/mhinz/neovim-remote" title="Neovim-remote"><img src="https://github.com/kosmatov/rbox/blob/master/samples/nvr-logo.png" height="100"></a>
<a href="https://java.com" title="Java"><img src="https://github.com/kosmatov/rbox/blob/master/samples/java-logo.png" height="100"></a>
<a href="https://nginx.org" title="Nginx"><img src="https://github.com/kosmatov/rbox/blob/master/samples/nginx-logo.png" height="100"></a>
<a href="https://docker.io" title="Docker"><img src="https://github.com/kosmatov/rbox/blob/master/samples/docker-logo.png" height="100"></a>
<a href="https://github.com/kosmatov/dotfiles" title="Dotfiles"><img src="https://github.com/kosmatov/rbox/blob/master/samples/dotfiles-logo.png" height="100"></a>
